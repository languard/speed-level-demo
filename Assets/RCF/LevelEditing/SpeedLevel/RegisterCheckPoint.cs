﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterCheckPoint : MonoBehaviour
{
    [SerializeField]
    int checkpointID = 1;

    [SerializeField]
    GameObject replacement;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            GameLogic.tc.RegisterCheckpoint(checkpointID);

            if(replacement != null)
            {
                GameObject.Instantiate(replacement, this.transform);
            }
            Destroy(this.gameObject);
            
        }
    }
}
