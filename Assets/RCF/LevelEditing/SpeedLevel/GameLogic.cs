﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogic : MonoBehaviour
{
    public static TimeControl tc;
    // Start is called before the first frame update
    void Start()
    {
        Scene timeScene = SceneManager.GetSceneByName("TimeScene");
        if(!timeScene.isLoaded)
        {
            SceneManager.sceneLoaded += TimeLoaded;
            SceneManager.LoadSceneAsync("TimeScene", LoadSceneMode.Additive);
            
        }
    }

    private void TimeLoaded(Scene arg0, LoadSceneMode arg1)
    {
        tc = GameObject.Find("TimeControl").GetComponent<TimeControl>();
        tc.InitTimeControl();
        tc.SwitchToGameMode();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        SceneManager.LoadScene("SpeedLevelScene");
    }
}
