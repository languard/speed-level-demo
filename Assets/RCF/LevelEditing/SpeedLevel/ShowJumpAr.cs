﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowJumpAr : MonoBehaviour
{

    public float walkSpeed = 6;
    public float runSpeed = 8;
    public float jumpSpeed = 8;
    public float gravity = 20;
    public float timeScale = 0.1f;
    public float maxTime = 4;

    public Color walkColor = Color.green;
    public float walkRadius = 0.1f;
    public Color runColor = Color.blue;
    public float runRadius = 0.1f;

    public Vector3[] walkPoints;
    Vector3[] runPoints;
    // Start is called before the first frame update
    void Start()
    {
        OnValidate();
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void CalculatePoints()
    {

    }

    void ResetArray(ref Vector3[] data)
    {
        data = new Vector3[Mathf.CeilToInt(maxTime / timeScale)];
        for(int i=0; i<data.Length; i++)
        {
            data[i] = Vector3.zero;
        }
    }

    private void OnValidate()
    {
        print("meep?");
        Vector3 curPos = this.transform.position;
        Vector3 offset = Vector3.zero;

        //do walk speed
        offset.z = walkSpeed;
        offset.y = jumpSpeed;
        ResetArray(ref walkPoints);

        for(int i=0; i<walkPoints.Length; i++)
        {
            curPos += offset * timeScale;
            offset.y -= gravity + timeScale;
            walkPoints[i] = curPos;
        }

        //do run speed
        curPos = this.transform.position;
        offset.z = runSpeed;
        offset.y = jumpSpeed;
        ResetArray(ref runPoints);

        for (int i = 0; i < runPoints.Length; i++)
        {
            curPos += offset * timeScale;
            offset.y -= gravity + timeScale;
            runPoints[i] = curPos;
        }

    }

    private void OnDrawGizmosSelected()
    {
        if (walkPoints.Length <= 0) return;

        Gizmos.color = walkColor;

        for(int i=0; i<walkPoints.Length; i++)
        {
            Gizmos.DrawSphere(walkPoints[i], walkRadius);
        }
    }
}
