﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeControl : MonoBehaviour
{
    [SerializeField]
    Text[] gameTimeTextFields;
    [SerializeField]
    Text txtBestTime;
    [SerializeField]
    Text txtCurrentTime;

    [SerializeField]
    Canvas gameCanvas;

    [SerializeField]
    Canvas mainMenuCanvas;

    System.TimeSpan bestTime;
    System.TimeSpan currentTime;
    System.TimeSpan levelStartTime;

    System.TimeSpan[] activeRun;

    [SerializeField]
    bool showTimeRankInGame = true;
    [SerializeField]
    bool showCheckpointsInMenu = true;
    

    bool runTimer = false;

    bool runOnce = true;

    // Start is called before the first frame update
    void Start()
    {
        InitTimeControl();
    }

    public void InitTimeControl()
    {
        if (!runOnce) return;
        runOnce = false;

        print("HItting START");
        txtBestTime.text = "--:--:--";
        txtCurrentTime.text = "--:--:--";

        bestTime = System.TimeSpan.MaxValue;

        if (showCheckpointsInMenu) gameCanvas.gameObject.SetActive(true);

        activeRun = new System.TimeSpan[gameTimeTextFields.Length];
    }

    // Update is called once per frame
    void Update()
    {
        if (!runTimer) return;

        activeRun[0] = System.TimeSpan.FromSeconds(Time.timeSinceLevelLoad) - levelStartTime;
        gameTimeTextFields[0].text = activeRun[0].ToString(@"mm\:ss\:ff");

        for(int i=1; i<activeRun.Length; i++)
        {
            if(activeRun[i] != System.TimeSpan.Zero)
            {
                gameTimeTextFields[i].text = activeRun[i].ToString(@"mm\:ss\:ff");
            }
        }
    }
    
    public void ShowOnlyGameCanvas()
    {
        gameCanvas.gameObject.SetActive(true);
        mainMenuCanvas.gameObject.SetActive(false);
    }

    public void ShowOnlyUICanvas()
    {
        gameCanvas.gameObject.SetActive(false);
        mainMenuCanvas.gameObject.SetActive(true);
    }

    public void ShowGameCanvas()
    {
        gameCanvas.gameObject.SetActive(true);
    }

    public void ShowUICanvas()
    {
        mainMenuCanvas.gameObject.SetActive(true);
    }

    public void ScoreRun()
    {
        if (!ValidateCheckpoints()) return;
        currentTime = activeRun[0];
        if(currentTime < bestTime)
        {
            bestTime = currentTime;
        }

        txtBestTime.text = bestTime.ToString(@"mm\:ss\:ff");
        txtCurrentTime.text = currentTime.ToString(@"mm\:ss\:ff");
        StopGameTime();
        ShowUICanvas();
        if (showCheckpointsInMenu) ShowGameCanvas();
    }

    public void ResetGameTimeText()
    {

        for(int i=0; i<gameTimeTextFields.Length; i++)
        {
            gameTimeTextFields[i].text = "--:--:--";
            activeRun[i] = System.TimeSpan.Zero;
        }
    }

    public void StartGameTime()
    {
        levelStartTime = System.TimeSpan.FromSeconds(Time.timeSinceLevelLoad);
        runTimer = true;
    }

    public void SwitchToGameMode()
    {
        ResetGameTimeText();
        StartGameTime();
        if (!showTimeRankInGame) mainMenuCanvas.gameObject.SetActive(false);
        gameCanvas.gameObject.SetActive(true);
    }

    public void StopGameTime()
    {
        runTimer = false;
    }

    public void RegisterCheckpoint(int checkPoint)
    {
        activeRun[checkPoint] = System.TimeSpan.FromSeconds(Time.timeSinceLevelLoad) - levelStartTime;
        gameTimeTextFields[checkPoint].text = activeRun[checkPoint].ToString(@"mm\:ss\:ff");
    }

    public bool ValidateCheckpoints()
    {
        bool result = true;

        for(int i=1; i<activeRun.Length; i++)
        {
            if (activeRun[i] == System.TimeSpan.Zero) result = false;
        }

        return result;
    }
}
