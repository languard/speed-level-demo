﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        Scene timeScene = SceneManager.GetSceneByName("TimeScene");
        if (!timeScene.isLoaded) SceneManager.LoadScene("TimeScene", LoadSceneMode.Additive);
    }

    public void _ClickQuit()
    {
        Application.Quit();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif

    }

    public void _StartClick()
    {
        SceneManager.LoadScene("GameScene", LoadSceneMode.Additive);
        SceneManager.UnloadSceneAsync("MainMenu");
    }

}
